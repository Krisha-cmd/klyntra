// Assuming welcomeMessage, optionsContainer, and logoutLink are already defined
const welcomeMessage = document.getElementById('welcome-message');
const optionsContainer = document.getElementById('options');
const logoutLink = document.getElementById('logout-link');

const getUrlParameter = (name) => {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(name);
  };

  let varValue = getUrlParameter('var');

  if (varValue) {
    localStorage.setItem('varValue', varValue);
    callCloudflareWorker(varValue)
  }
  else{
    varValue = localStorage.getItem('varValue');
    if(varValue){
    callCloudflareWorker(varValue)
    }
  }


  async function callCloudflareWorker(inputString) {
    try {
      const cloudflareWorkerURL = 'https://isqreregistered.krishaaggarwal-mit.workers.dev/'; 
  
      const response = await fetch(`${cloudflareWorkerURL}retrieve?code=${encodeURIComponent(inputString)}`);
      if (response.ok) {
        let result = await response.text();
        if(result==='Data not found'){
          result=null
          fetch('/check-login')
          .then(response => response.json())
          .then(data => {
            if (data.user) {
              storeInKV(inputString ,data.user.username)
              welcomeMessage.textContent = `Hello ${data.user.username}! QR added`;
              optionsContainer.style.display = 'none';
              logoutLink.style.display = 'block';
            }
            else{
                welcomeMessage.textContent = `Login or Signup to add QR`;
                optionsContainer.style.display = 'block';
                logoutLink.style.display = 'none'; 
            }
          });
        }
        else{
            localStorage.setItem('userName', result);
            window.location.href = '/login.html'
        }
        //what to do with the output

      } else {
        console.error('Error calling Cloudflare Worker:', response.status, response.statusText);
      }
    } catch (error) {
      console.error('Error:', error.message);
    }
  }


  async function storeInKV(code,data){
    const cloudflareWorkerURL = 'https://isqreregistered.krishaaggarwal-mit.workers.dev/';
    const response = await fetch(`${cloudflareWorkerURL}store?code=${encodeURIComponent(code)}&data=${data}`);
    if (response.ok) {
      let result = await response.text();
      console.log(result)
    }
  }