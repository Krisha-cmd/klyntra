document.addEventListener('DOMContentLoaded', () => {
    const logoutButton = document.getElementById('logout-btn');
  
    logoutButton.addEventListener('click', async () => {
      // Send a request to the server to perform logout
      const response = await fetch('/logout', { method: 'GET' });
  
      if (response.redirected) {
        // Redirect to the login page after logout
        window.location.href = response.url;
      } else {
        // Handle logout error (display an alert, for example)
        console.error('Logout failed');
      }
    });
  });
  