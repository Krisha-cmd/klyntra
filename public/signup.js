document.addEventListener('DOMContentLoaded', () => {
    const signupForm = document.getElementById('signup-form');
  
    signupForm.addEventListener('submit', async (event) => {
      event.preventDefault();
  
      const username = document.getElementById('username').value;
      const password = document.getElementById('password').value;
  
      // Send signup data to the server
      const response = await fetch('/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `username=${username}&password=${password}`,
      });
  
      if (response.redirected) {
        window.location.href = response.url;
        isLoggedIn=true;
      } else {
        // Handle signup error (display an alert, for example)
        console.error('Signup failed');
      }
    });
  });
  