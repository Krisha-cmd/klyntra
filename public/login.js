document.addEventListener('DOMContentLoaded', () => {
    const loginForm = document.getElementById('login-form');

    if (localStorage.getItem('userName')) {
      const userNameValue = localStorage.getItem('userName');
      document.getElementById('username').value = userNameValue;
    }
  
    loginForm.addEventListener('submit', async (event) => {
      event.preventDefault();
  
      const username = document.getElementById('username').value;
      const password = document.getElementById('password').value;
  
      // Send login data to the server
      const response = await fetch('/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `username=${username}&password=${password}`,
      });
  
      if (response.redirected) {
        window.location.href = response.url;
        isLoggedIn=true;
      } else {
        // Handle login error (display an alert, for example)
        console.error('Login failed');
      }
    });
  });
  