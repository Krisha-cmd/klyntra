import os
import uuid
import qrcode

def generate_qr_code(data, file_path):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(data)
    qr.make(fit=True)

    img = qr.make_image(fill_color="black", back_color="white")
    img.save(file_path)

def generate_and_save_qr_codes(n):
    if not os.path.exists("qr_codes"):
        os.makedirs("qr_codes")

    for i in range(n):

        unique_id = str(uuid.uuid4())[:6]
        data=f"https://quickstart-2e2f5787.myshopify.com/?var={unique_id}"
        file_path = f"qr_codes/qr_code_{i+1}.png"
        generate_qr_code(data, file_path)
        print(f"QR code {i+1} generated and saved as {file_path}")

if __name__ == "__main__":
    num_qr_codes = 20  

    generate_and_save_qr_codes(num_qr_codes)
