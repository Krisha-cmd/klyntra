from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend
from base64 import b64encode
import os

def generate_aes_128_key():
    key = os.urandom(16)
    return key


def bytes_to_base64(data):
    return b64encode(data).decode('utf-8')


aes_key = generate_aes_128_key()
base64_key = bytes_to_base64(aes_key)

print("Generated AES-128 key (base64):", base64_key)
