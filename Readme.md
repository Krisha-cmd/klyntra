## Cloudflare workers

So the idea is,

(redirect and localstorage may not be required actually)
(but local storage may be required if we want user to log in)
you have a url which goes like klyntra.com?qr=fhwjfc
extract parameters if none in local storage or if new parameters different from old

we first check if the qr code is already registered?
if it is user id!=null:
    login using that

else user id == null
    if user is logged in,
        store the qr and loggedin user id,
    else
        store params in local storage
        ask if they want to store qr code (login)


Thus functions:
fetchParams
user_id=isQrRegistered(); (cloudflare workers)
if(user_id):login
else: storeQR() (cloudflare workers)
successfullyStored()

two functions: isQrRegistered(), storeQR()