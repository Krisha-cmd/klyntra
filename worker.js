//bind it using variable settings
const kvNamespace = Qr_User;

addEventListener('fetch', (event) => {
  event.respondWith(handleRequest(event.request));
});

async function handleRequest(request) {
  const url = new URL(request.url);
  const path = url.pathname;

  if (path.startsWith('/store')) {
    const code = url.searchParams.get('code');
    const data = url.searchParams.get('data');

    if (code && data) {
      await kvNamespace.put(code, data);
      return new Response('Data stored successfully', { status: 200 });
    } else {
      return new Response('Invalid parameters', { status: 400 });
    }
  }

  if (path.startsWith('/retrieve')) {
    const code = url.searchParams.get('code');
    if (code) {
      const storedData = await kvNamespace.get(code);
      return new Response(storedData || 'Data not found', { status: 200 });
    } else {
      return new Response('Invalid parameters', { status: 400 });
    }
  }

  return new Response('Not Found', { status: 404 });
}
