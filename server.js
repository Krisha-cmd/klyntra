const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const axios = require('axios');
const cors = require('cors');
require('dotenv').config();

const app = express();

// Connect to MongoDB Atlas
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

// Define User Schema
const userSchema = new mongoose.Schema({
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
});

const User = mongoose.model('User', userSchema);

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: 'your-secret-key', resave: true, saveUninitialized: true }));

// Serve static files (HTML, CSS, JS)
app.use(express.static('public'));
app.use(cors());

// Routes
app.post('/login', async (req, res) => {
  const { username, password } = req.body;

  // Check credentials
  const user = await User.findOne({ username, password });

  if (user) {
    req.session.user = user;
    res.redirect('/');
  } else {
    res.redirect('/login');
  }
});

app.post('/signup', async (req, res) => {
  const { username, password } = req.body;

  // Create a new user
  const newUser = new User({ username, password });
  await newUser.save();

  req.session.user = newUser;
  res.redirect('/');
});

app.get('/logout', (req, res) => {
  req.session.destroy();
  res.redirect('/login');
});

// app.get('/home', (req, res) => {
//   if (req.session.user) {
//     res.sendFile(__dirname + '/public/home.html');
//   } else {
//     res.redirect('/login');
//   }
// });

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/home.html');
  });

app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/public/login.html');
});

app.get('/signup', (req, res) => {
  res.sendFile(__dirname + '/public/signup.html');
});



app.get('/check-login', (req, res) => {
    const loggedInUser = req.session.user;
    
    if (loggedInUser) {
        res.json({ user: loggedInUser });
    } else {
        res.json({ user: null });
    }
});


app.get('/api/getOrder', async (req, res) => {
  try {
      const response = await axios.post(
          'https://api.razorpay.com/v1/orders',
          {
              amount: 50000,
              currency: 'INR',
              receipt: 'receipt#1',
              notes: {
                  key1: 'value3',
                  key2: 'value2'
              }
          },
          {
              headers: {
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + Buffer.from(process.env.RAZORPAY_KEY_ID + ':' + process.env.RAZORPAY_API).toString('base64')
              }
          }
      );
      res.json(response.data);
      console.log(response.data);
  } catch (error) {
      console.error('Razorpay API Request Error:', error.message);
      res.status(500).json({ error: 'Internal Server Error' });
  }
});


const staticCardDetails = {
  number: '4111111111111111',
  expiration_month: '12',
  expiration_year: '2023',
  cvv: '123',
}; 

const razorpayApiKey = process.env.RAZORPAY_API;

if (!razorpayApiKey) {
  console.error('Razorpay API key is missing.');
  process.exit(1);
}

app.get('/api/getToken', async (req, res) => {
  try {
    const tokenResponse = await fetch('https://api.razorpay.com/v1/tokens', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${razorpayApiKey}`,
      },
      body: JSON.stringify(staticCardDetails),
    });

    const tokenData = await tokenResponse.json();

    // Handle the tokenData as needed

    res.json({ tokenData });
  } catch (error) {
    console.error('Error:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});




// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});